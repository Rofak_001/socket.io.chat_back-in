package com.hrdcenter.com.springchatsocketio.repository;

import com.hrdcenter.com.springchatsocketio.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRep extends JpaRepository<User, Integer> {
}
