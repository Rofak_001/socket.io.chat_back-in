package com.hrdcenter.com.springchatsocketio.repository;

import com.hrdcenter.com.springchatsocketio.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRep extends JpaRepository<Message, Integer>
{
    @Query("SELECT m FROM Message as m WHERE m.roomNumber=:senderNameId OR m.roomNumber=:receiverNameId")
    List<Message> findAllMessageByRoom(@Param("senderNameId") String senderNameId,@Param("receiverNameId") String receiverNameId);
}
