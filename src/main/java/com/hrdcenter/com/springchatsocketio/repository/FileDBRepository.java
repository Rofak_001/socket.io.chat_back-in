package com.hrdcenter.com.springchatsocketio.repository;

import com.hrdcenter.com.springchatsocketio.model.FileDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, String> {
}
