package com.hrdcenter.com.springchatsocketio.controller;

import com.hrdcenter.com.springchatsocketio.model.Message;
import com.hrdcenter.com.springchatsocketio.repository.MessageRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageController {
    private MessageRep messageRep;

    @Autowired
    public void setMessageRep(MessageRep messageRep) {
        this.messageRep = messageRep;
    }

    @GetMapping("/messages")
    public List<Message> findAllBySenderIdAndReceiverIdOrderByDate(@RequestParam String senderNameId,@RequestParam String receiverNameId) {
        return messageRep.findAllMessageByRoom(senderNameId,receiverNameId);
    }
}
