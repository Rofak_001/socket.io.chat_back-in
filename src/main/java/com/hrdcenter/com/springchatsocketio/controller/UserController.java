package com.hrdcenter.com.springchatsocketio.controller;

import com.hrdcenter.com.springchatsocketio.model.User;
import com.hrdcenter.com.springchatsocketio.repository.UserRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private UserRep userRep;

    @Autowired
    public void setUserRep(UserRep userRep) {
        this.userRep = userRep;
    }

    @GetMapping("/users")
    public List<User> findAll() {
        return userRep.findAll();
    }
    @PostMapping("/users")
    @CrossOrigin
    public ResponseEntity<User> add(@RequestBody User user) {
        userRep.save(user);
        return ResponseEntity.ok(user);
    }
}
