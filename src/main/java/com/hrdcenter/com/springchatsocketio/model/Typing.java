package com.hrdcenter.com.springchatsocketio.model;

import java.util.UUID;

public class Typing {
    private UUID senderId;
    private String sender;
    private String receiverName;
    public Typing(){}

    public Typing(UUID senderId, String sender, String receiverName) {
        this.senderId = senderId;
        this.sender = sender;
        this.receiverName = receiverName;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
