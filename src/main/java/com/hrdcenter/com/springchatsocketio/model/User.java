package com.hrdcenter.com.springchatsocketio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String sessionId;
    private String username;

    public User() {

    }

    public User(String sessionId, String username) {
        this.sessionId = sessionId;
        this.username = username;
    }

    public int getId() {
        return Id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User [sessionId=" + sessionId + ", username=" + username + "]";
    }

}
